package util

import (
	"math/big"
)

type ConversionRates map[string]map[string]*big.Rat

var conversionRates ConversionRates

func init() {
	conversionRates = ConversionRates{
		"GBP": {
			"USD": big.NewRat(8, 10), // .8
			"EUR": big.NewRat(9, 10), // .9
		},
		"EUR": {
			"USD": big.NewRat(9, 10),  // .9
			"GBP": big.NewRat(11, 10), // 1.1
		},
		"USD": {
			"EUR": big.NewRat(11, 10), // 1.1
			"GBP": big.NewRat(12, 10), // 1.2
		},
	}
}

func GetConversionRates() ConversionRates {
	return conversionRates
}
