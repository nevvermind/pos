package util

import (
	"math/big"
	"testing"
)

func TestPercentage(t *testing.T) {
	if GetPercentageFromInt64(100, big.NewRat(10, 1)).Cmp(big.NewRat(10, 1)) != 0 {
		t.Errorf("10%% -> 100 = 10")
	}

	if GetPercentageFromInt64(110, big.NewRat(50, 1)).Cmp(big.NewRat(55, 1)) != 0 {
		t.Errorf("50%% -> 110 = 55")
	}

	if GetPercentageFromInt64(165, big.NewRat(175, 10)).Cmp(big.NewRat(28875, 1000)) != 0 {
		t.Errorf("17.5%% -> 165 = 28.875")
	}
}
