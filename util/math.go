package util

import (
	"math/big"
	"strconv"
)

func RatRoundUp(rat *big.Rat) int64 {

	result := rat

	if !result.IsInt() {
		result = new(big.Rat).Add(rat, big.NewRat(5, 10)) // + .5
	}

	return StrToInt64(result.FloatString(0))
}

func RatRoundDown(rat *big.Rat) int64 {

	result := rat

	if !result.IsInt() {
		result = new(big.Rat).Sub(rat, big.NewRat(5, 10)) // - .5
	}

	return StrToInt64(result.FloatString(0))
}

func StrToInt64(value string) int64 {
	i, err := strconv.ParseInt(value, 10, 64)

	if err != nil {
		panic(err)
	}

	return i
}

func GetPercentageFromInt64(value int64, percentage *big.Rat) *big.Rat {
	return GetPercentageFromBigRat(new(big.Rat).SetInt64(value), percentage)
}

func GetPercentageFromBigRat(value *big.Rat, percentage *big.Rat) *big.Rat {
	return new(big.Rat).Mul(value, new(big.Rat).Quo(percentage, new(big.Rat).SetInt64(100)))
}
