package main

import (
	"fmt"
	"math/big"
	"moti/intelligentpos_test/model"
	"moti/intelligentpos_test/util"
)

func main() {

	// Create a quote having GBP as the base currency, and using a hard-coded exchange rate
	quote := model.NewQuote(model.NewGbp(), util.GetConversionRates())

	// create a product with a sku "sku1", price of 100 GBP and add a tax of 10%
	product1 := model.NewProduct("sku1", model.NewMoney(100, model.NewGbp()))
	taxRate1, _ := model.NewTaxRate("tax1", new(big.Rat).SetInt64(10)) // 10%
	product1.AddTaxRate(taxRate1)

	// create another product with a sku "sku2", a price of 200 USD,
	// and add 2 tax rates to it, of 50% and 20%
	product2 := model.NewProduct("sku2", model.NewMoney(200, model.NewUsd()))
	taxRate2, _ := model.NewTaxRate("tax1", new(big.Rat).SetInt64(50)) // 50%
	taxRate3, _ := model.NewTaxRate("tax2", new(big.Rat).SetInt64(20)) // 20%
	product2.AddTaxRate(taxRate2)
	product2.AddTaxRate(taxRate3)

	// create a product costing 200 GBP, with no tax
	product3 := model.NewProduct("sku3", model.NewMoney(200, model.NewGbp()))

	// populate the quote
	quote.AddProduct(product1)
	quote.AddProduct(product2)
	quote.AddProduct(product3)

	baseCurrencyCode := quote.GetBaseCurrency().GetCode()

	lineBreak := func() {
		fmt.Printf("----------\n")
	}

	lineBreak()

	product2WithTaxPrice := product2.GetPriceWithTaxes()
	product2WithoutTaxPrice := product2.GetPrice()
	fmt.Printf("Product2 with taxes: %d %s\n", product2WithTaxPrice.GetAmount(), product2WithTaxPrice.GetCurrency().GetCode())
	fmt.Printf("Product2 without taxes: %d %s\n", product2WithoutTaxPrice.GetAmount(), product2WithoutTaxPrice.GetCurrency().GetCode())

	lineBreak()

	// tax-breakdown per product
	fmt.Printf("Taxes for product 1:\n")
	for taxRateLabel, taxTotal := range product1.GetTaxBreakdown() {
		fmt.Printf("- %s: %d %s\n", taxRateLabel, taxTotal.GetAmount(), taxTotal.GetCurrency().GetCode())
	}

	fmt.Printf("Taxes for product 2:\n")
	for taxRateLabel, taxTotal := range product2.GetTaxBreakdown() {
		fmt.Printf("- %s: %d %s\n", taxRateLabel, taxTotal.GetAmount(), taxTotal.GetCurrency().GetCode())
	}

	lineBreak()

	// querying the quote
	fmt.Printf("Base currency of the quote: %s\n", baseCurrencyCode)
	fmt.Printf("Grand total of the quote (including taxes): %d %s\n", quote.GetGrandTotal().GetAmount(), baseCurrencyCode)
	fmt.Printf("Subtotal of the quote (excluding taxes): %d %s\n", quote.GetSubtotal().GetAmount(), baseCurrencyCode)
	fmt.Printf("Total taxes in %s: %d\n", baseCurrencyCode, quote.GetGrandTotal().GetAmount()-quote.GetSubtotal().GetAmount())

	lineBreak()

	// total tax break-down
	for taxRateLabel, taxTotal := range quote.GetTaxBreakdown() {
		fmt.Printf("Total for Tax code \"%s\": %d %s\n", taxRateLabel, taxTotal.GetAmount(), taxTotal.GetCurrency().GetCode())
	}
}
