package model

import (
	"errors"
	"math/big"
)

type TaxRate interface {
	GetLabel() TaxRateLabel
	GetRate() *big.Rat
	GetPostCode() string
}

type TaxRateLabel string

func NewTaxRate(label TaxRateLabel, rate *big.Rat) (*taxRate, error) {
	if rate.Sign() < 0 {
		return nil, errors.New("Tax rate cannot be negative")
	}
	return &taxRate{label, rate}, nil
}

type taxRate struct {
	label TaxRateLabel
	rate  *big.Rat
}

func (t *taxRate) GetRate() *big.Rat {
	return t.rate
}
func (t *taxRate) GetLabel() TaxRateLabel {
	return t.label
}

func (t *taxRate) GetPostCode() string {
	return "" // currently unused, but just to show that Tax rate usually depends on the location
}
