package model

import (
	"math/big"
	"moti/intelligentpos_test/util"
	"testing"
)

var conversionRates = util.ConversionRates{
	"USD": {
		"EUR": big.NewRat(9, 10), // .9
		"GBP": big.NewRat(4, 10), // .4
	},
}

func TestZeroTotals(t *testing.T) {

	quote := NewQuote(NewGbp(), conversionRates)

	if zeroSubtotal := quote.GetSubtotal(); zeroSubtotal.amount != 0 {
		t.Error("Subtotal is not 0")
	}

	if zeroGrandTotal := quote.GetGrandTotal(); zeroGrandTotal.amount != 0 {
		t.Error("Grand total is not 0")
	}
}

func TestNonZeroTotals(t *testing.T) {
	quote := NewQuote(NewGbp(), conversionRates)

	product1 := NewProduct("sku1", NewMoney(100, NewGbp()))
	taxRate1, _ := NewTaxRate("tax1", new(big.Rat).SetInt64(10)) // 10%
	product1.AddTaxRate(taxRate1)

	product2 := NewProduct("sku2", NewMoney(200, NewGbp()))
	taxRate2, _ := NewTaxRate("tax1", new(big.Rat).SetInt64(50)) // 50%
	taxRate3, _ := NewTaxRate("tax2", new(big.Rat).SetInt64(20)) // 20%
	product2.AddTaxRate(taxRate2)
	product2.AddTaxRate(taxRate3)

	quote.AddProduct(product1)
	quote.AddProduct(product2)

	if subtotal := quote.GetSubtotal(); subtotal.amount != 300 {
		t.Error("Subtotal is not 300")
	}

	if grandTotal := quote.GetGrandTotal(); grandTotal.amount != 470 {
		t.Error("Grand total is not 470")
	}
}

func TestTotalsForProductsWithDifferentCurrencies(t *testing.T) {

	// base currency set to GBP
	quote := NewQuote(NewGbp(), conversionRates)

	product1 := NewProduct("sku1", NewMoney(100, NewGbp()))
	taxRate1, _ := NewTaxRate("tax1", new(big.Rat).SetInt64(10)) // 10%
	product1.AddTaxRate(taxRate1)

	product2 := NewProduct("sku2", NewMoney(200, NewUsd()))      // 200 USD * 0.4 = 80 GBP
	taxRate2, _ := NewTaxRate("tax1", new(big.Rat).SetInt64(10)) // 10%
	product2.AddTaxRate(taxRate2)

	quote.AddProduct(product1)
	quote.AddProduct(product2)

	if subtotal := quote.GetSubtotal(); subtotal.amount != 180 {
		t.Errorf("Subtotal is not 180. Got %d instead.", quote.GetSubtotal().GetAmount())
	}

	if grandTotal := quote.GetGrandTotal(); grandTotal.amount != 198 {
		t.Errorf("Grand total is not 198. Got %d instead.", quote.GetGrandTotal().GetAmount())
	}
}

func TestTaxBreakdownWithDifferentCurrencies(t *testing.T) {
	// base currency set to GBP
	quote := NewQuote(NewGbp(), conversionRates)

	product1 := NewProduct("sku1", NewMoney(100, NewGbp()))
	taxRate1, _ := NewTaxRate("tax1", new(big.Rat).SetInt64(10)) // 10%
	taxRate2, _ := NewTaxRate("tax2", new(big.Rat).SetInt64(20)) // 20%
	product1.AddTaxRate(taxRate1)
	product1.AddTaxRate(taxRate2)

	product2 := NewProduct("sku2", NewMoney(200, NewUsd()))      // 200 USD * 0.4 = 80 GBP
	taxRate3, _ := NewTaxRate("tax1", new(big.Rat).SetInt64(10)) // 10%
	product2.AddTaxRate(taxRate3)

	product3 := NewProduct("sku3", NewMoney(200, NewGbp()))

	quote.AddProduct(product1)
	quote.AddProduct(product2)
	quote.AddProduct(product3)

	taxBreakdown := quote.GetTaxBreakdown()

	if taxBreakdown["tax1"].GetAmount() != 18 {
		t.Errorf("Tax1 is not 18. Got %d instead.", taxBreakdown["tax1"].GetAmount())
	}

	if taxBreakdown["tax2"].GetAmount() != 22 {
		t.Errorf("Tax2 is not 22. Got %d instead.", taxBreakdown["tax2"].GetAmount())
	}
}
