package model

type Currency interface {
	GetCode() string

	// The denominator represents the number that we need to divide the amount with in order to get
	// to the single unit of currency.
	//
	// As we store money amounts in their indivisible sub-units (cent, pence etc.), if we want to display
	// te actual currency unit, we'll need the denominator. This is mostly for a graphical representation
	// of the currency, and it's not used in the currency calculations.
	//
	// E.g. An EURO has a denominator of 100 (cents), and so has the USD and GBP.
	// A YEN would have a denominator of 1.
	GetDenominatorAmount() int
}

type currency struct {
	code              string
	denominatorAmount int
}

func (c *currency) GetCode() string {
	return c.code
}

func (c *currency) GetDenominatorAmount() int {
	return c.denominatorAmount
}

func NewCurrency(code string, denominatorAmount int) *currency {
	return &currency{code, denominatorAmount}
}

func NewGbp() *currency {
	return NewCurrency("GBP", 100)
}

func NewEur() *currency {
	return NewCurrency("EUR", 100)
}

func NewUsd() *currency {
	return NewCurrency("USD", 100)
}
