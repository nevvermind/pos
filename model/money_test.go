package model

import (
	"math/big"
	"moti/intelligentpos_test/util"
	"testing"
)

func TestCorrectConversion(t *testing.T) {

	conversionRates := util.ConversionRates{
		"FOO": {
			"BAR": big.NewRat(5, 10),  // .5
			"BAZ": big.NewRat(4, 10),  // .4
			"BUZ": big.NewRat(10, 10), // 1
		},
	}

	foo := NewMoney(44, NewCurrency("FOO", 1))

	// FOO
	if foo, _ := foo.ConvertTo(NewCurrency("FOO", 1), conversionRates); foo.GetAmount() != 44 {
		t.Errorf("Expected 44 FOOs to be converted in 44 FOOs, but got %d FOOs instead", foo.GetAmount())
	}

	// BAR
	if bar, _ := foo.ConvertTo(NewCurrency("BAR", 1), conversionRates); bar.GetAmount() != 22 {
		t.Errorf("Expected 44 FOOs to be converted in 22 BARs, but got %d BARs instead", bar.GetAmount())
	}

	// BAZ
	if baz, _ := foo.ConvertTo(NewCurrency("BAZ", 1), conversionRates); baz.GetAmount() != 18 {
		t.Errorf("Expected 44 FOOs to be converted in 18 BAZs, but got %d BAZs instead", baz.GetAmount())
	}

	// BUZ
	if buz, _ := foo.ConvertTo(NewCurrency("BUZ", 1), conversionRates); buz.GetAmount() != 44 {
		t.Errorf("Expected 44 FOOs to be converted in 44 BUZs, but got %d BUZs instead", buz.GetAmount())
	}
}
