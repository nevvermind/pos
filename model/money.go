package model

import (
	"errors"
	"fmt"
	"math/big"
	"moti/intelligentpos_test/util"
)

type Money interface {
	GetCurrency() Currency

	// int64 was chosen as it's easier to work with, as opposed to big.Rat.
	// Attention should be given for large sums, to not overflow
	GetAmount() int64

	// API for converting between currencies, based on an external conversion rate table.
	// Conversion is always rounded UP, as we cannot have decimals for amounts representing an indivisible unit.
	ConvertTo(currency Currency, conversionRates util.ConversionRates) (Money, error)
}

func NewMoney(amount int64, currency Currency) *money {
	return &money{amount, currency}
}

type money struct {
	amount int64

	currency Currency
}

func (m *money) GetCurrency() Currency {
	return m.currency
}

func (m *money) GetAmount() int64 {
	return m.amount
}

func (m *money) ConvertTo(targetCurrency Currency, conversionRates util.ConversionRates) (*money, error) {

	// same currency; return same value, but as a new struct
	if m.currency.GetCode() == targetCurrency.GetCode() {
		return NewMoney(m.amount, m.currency), nil
	}

	rates, ok := conversionRates[m.currency.GetCode()]

	if !ok {
		return nil, errors.New(fmt.Sprintf("Cannot convert currency. Unknown code: %s", m.currency.GetCode()))
	}

	rate, ok := rates[targetCurrency.GetCode()]

	if !ok {
		return nil, errors.New(fmt.Sprintf("Cannot convert from %s to %s", m.currency.GetCode(), targetCurrency.GetCode()))
	}

	result := new(big.Rat).Mul(rate, new(big.Rat).SetInt64(m.amount))

	// round UP, if necessary
	return NewMoney(util.RatRoundUp(result), targetCurrency), nil
}
