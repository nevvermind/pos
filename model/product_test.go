package model

import (
	"math/big"
	"testing"
)

func TestCorrectPriceWithTaxes(t *testing.T) {

	product1 := NewProduct("sku1", NewMoney(100, NewGbp()))

	taxRate1, _ := NewTaxRate("tax1", new(big.Rat).SetInt64(10)) // 10%
	product1.AddTaxRate(taxRate1)

	if product1.GetPriceWithTaxes().GetAmount() != (100 + 10) {
		t.Errorf("Was expecting %d, but got %d instead", 100+10, product1.GetPriceWithTaxes().GetAmount())

	}

	taxRate2, _ := NewTaxRate("tax2", new(big.Rat).SetInt64(50)) // 50%
	product1.AddTaxRate(taxRate2)

	if product1.GetPriceWithTaxes().GetAmount() != (100 + 10 + 55) {
		t.Errorf("Was expecting %d, but got %d instead", 100+10+55, product1.GetPriceWithTaxes().GetAmount())
	}

	taxRate3, _ := NewTaxRate("tax3", big.NewRat(175, 10)) // 17.5%
	product1.AddTaxRate(taxRate3)

	if product1.GetPriceWithTaxes().GetAmount() != 194 {
		t.Errorf("Was expecting %d, but got %d instead", 194, product1.GetPriceWithTaxes().GetAmount())
	}

	// price remains the same
	if product1.GetPrice().GetAmount() != 100 {
		t.Errorf("Was expecting %d, but got %d instead", 100, product1.GetPrice().GetAmount())
	}
}

func TestCorrectTaxBreakdown(t *testing.T) {
	product1 := NewProduct("sku1", NewMoney(100, NewGbp()))

	taxRate1, _ := NewTaxRate("tax1", new(big.Rat).SetInt64(10)) // 10%
	taxRate2, _ := NewTaxRate("tax2", new(big.Rat).SetInt64(50)) // 50%
	taxRate3, _ := NewTaxRate("tax3", big.NewRat(175, 10))       // 17.5%

	product1.AddTaxRate(taxRate1)
	product1.AddTaxRate(taxRate2)
	product1.AddTaxRate(taxRate3)

	taxBreakdown := product1.GetTaxBreakdown()

	if len(taxBreakdown) != 3 {
		t.Errorf("Invalid number of tax breakdown elements")
	}

	if taxBreakdown["tax1"].GetAmount() != 10 {
		t.Errorf("Invalid tax1 amount. Expected %d, but got %d", 10, taxBreakdown["tax1"].GetAmount())
	}

	if taxBreakdown["tax2"].GetAmount() != 55 {
		t.Errorf("Invalid tax2 amount. Expected %d, but got %d", 55, taxBreakdown["tax2"].GetAmount())
	}

	if taxBreakdown["tax3"].GetAmount() != 29 {
		t.Errorf("Invalid tax3 amount. Expected %d, but got %d", 29, taxBreakdown["tax3"].GetAmount())
	}
}
