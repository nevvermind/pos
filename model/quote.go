package model

import (
	"container/list"
	"moti/intelligentpos_test/util"
)

type Quote interface {
	AddProduct(product Product)

	// A fresh and ordered list.
	// N.B. For better or worse, Go doesn't guarantee the *iteration* order on a map.
	GetProducts() map[Sku]Product

	// A base currency is necessary, to handle the scenario in which Products have different currencies.
	// This usually is the currency in the region of the store's physical location.
	// All totals are computed as given in the base currency.
	GetBaseCurrency() Currency

	// Including taxes
	GetGrandTotal() Money

	// Excluding taxes
	GetSubtotal() Money

	// Always a fresh list, to maintain immutability.
	GetTaxBreakdown() map[TaxRateLabel]*money
}

func NewQuote(baseCurrency Currency, conversionRates util.ConversionRates) *quote {
	return &quote{baseCurrency: baseCurrency, conversionRates: conversionRates}
}

type quote struct {
	products        list.List
	baseCurrency    Currency
	conversionRates util.ConversionRates
}

func (q *quote) GetBaseCurrency() Currency {
	return q.baseCurrency
}

func (q *quote) toBasePrice(price *money) *money {
	basePrice, err := price.ConvertTo(q.baseCurrency, q.conversionRates)
	if err != nil {
		panic(err)
	}
	return basePrice
}

func (q *quote) GetGrandTotal() *money {

	var taxTotalAmount int64 = 0

	// calculate tax total
	for _, product := range q.GetProducts() {

		amountBasePriceWithTaxes := q.toBasePrice(product.GetPriceWithTaxes()).GetAmount()
		amountBasePrice := q.toBasePrice(product.GetPrice()).GetAmount()

		// extract the tax from this product, by diff
		taxTotalAmount += amountBasePriceWithTaxes - amountBasePrice
	}

	return NewMoney(q.GetSubtotal().GetAmount()+taxTotalAmount, q.baseCurrency)

}

func (q *quote) GetSubtotal() *money {
	var sum int64 = 0
	for _, product := range q.GetProducts() {
		// convert to base currency, without tax
		sum += q.toBasePrice(product.GetPrice()).GetAmount()
	}
	return NewMoney(sum, q.baseCurrency)
}

// There's no check for Products already in the quote, as the qty is always assumed to be 1
func (q *quote) AddProduct(product *product) {
	q.products.PushFront(product)
}

func (q *quote) GetProducts() map[Sku]Product {

	products := make(map[Sku]Product)

	for e := q.products.Front(); e != nil; e = e.Next() {
		product := e.Value.(Product)
		products[product.GetSku()] = product
	}

	return products
}

func (q *quote) GetTaxBreakdown() map[TaxRateLabel]*money {
	breakdown := make(map[TaxRateLabel]*money)

	for _, product := range q.GetProducts() {

		for taxRateLabel, singleProductTax := range product.GetTaxBreakdown() {
			taxTotal, ok := breakdown[taxRateLabel]

			if !ok {
				breakdown[taxRateLabel] = q.toBasePrice(singleProductTax)
			} else {
				basePriceSingleProductTax := q.toBasePrice(singleProductTax)
				breakdown[taxRateLabel] = q.toBasePrice(NewMoney(taxTotal.GetAmount()+basePriceSingleProductTax.GetAmount(), q.GetBaseCurrency()))
			}

		}
	}

	return breakdown
}
