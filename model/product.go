package model

import (
	"container/list"
	"fmt"
	"math/big"
	"moti/intelligentpos_test/util"
)

type Product interface {
	GetSku() Sku
	GetPrice() *money

	// Always rounded up
	GetPriceWithTaxes() *money

	// Guarantees uniqueness
	AddTaxRate(t *taxRate)

	// always a fresh list, to maintain immutability
	GetTaxBreakdown() TaxBreakdown
}

func NewProduct(sku Sku, price *money) *product {
	return &product{sku: sku, price: price}
}

type TaxBreakdown map[TaxRateLabel]*money

type Sku string

type product struct {
	price    *money
	taxRates list.List
	sku      Sku
}

func (p *product) GetSku() Sku {
	return p.sku
}

func (p *product) GetPrice() *money {
	return p.price
}

func (p *product) GetPriceWithTaxes() *money {

	productPrice := p.GetPrice()
	sum := new(big.Rat).SetInt64(productPrice.GetAmount())

	// use the tax breakdown to calculate the tax total
	var taxTotal int64 = 0

	for _, tax := range p.GetTaxBreakdown() {
		taxTotal += tax.GetAmount()
	}

	taxTotalRat := new(big.Rat).SetInt64(taxTotal)

	// round UP, if necessary
	return NewMoney(util.RatRoundUp(sum.Add(sum, taxTotalRat)), productPrice.GetCurrency())
}

// Tax rates are stored in queue (FIFO)
func (p *product) AddTaxRate(t *taxRate) {

	// check if the tax rate code already exists for this product
	for e := p.taxRates.Front(); e != nil; e = e.Next() {
		if t.GetLabel() == e.Value.(TaxRate).GetLabel() {
			panic(fmt.Sprintf("Tax rate code %s already exists", t.GetLabel()))
		}
	}

	p.taxRates.PushBack(t)
}

func (p *product) GetTaxBreakdown() TaxBreakdown {

	breakdown := make(TaxBreakdown)

	productPriceAsMoney := p.GetPrice()
	productPrice := new(big.Rat).SetInt64(productPriceAsMoney.GetAmount())

	for e := p.taxRates.Front(); e != nil; e = e.Next() {
		taxRate := e.Value.(TaxRate)
		taxPrice := util.GetPercentageFromBigRat(productPrice, taxRate.GetRate())

		breakdown[taxRate.GetLabel()] = NewMoney(util.RatRoundUp(taxPrice), productPriceAsMoney.GetCurrency())

		// now increase the products price with the current tax,
		// so that the next computation will be relative to it
		productPrice.Add(productPrice, taxPrice)
	}

	return breakdown
}
